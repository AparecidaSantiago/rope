export default{
    port: process.env.PORT || 3000,
    secretKey: process.env.SECRETKEY || '93f22f663a8e8884d7da738d62594c502f1c190e',
    publicRoutes: process.env.PUBLICROUTES || [
        'users/create',
        'users/auth'
    ]
}