import {UserController} from "./controller/UserController";
import {RelatorioController} from "./controller/RelatorioController";
import {AgenteController} from "./controller/AgenteController";
import {PerguntaController} from "./controller/PerguntaController";
import { RespostaController } from "./controller/RespostaController";
import { SetorController } from "./controller/SetorController";

export const Routes = [
    { method: "get",route: "/users", controller: UserController, action: "all"}, 
    { method: "get",route: "/users/:id",controller: UserController,action: "one"},
    { method: "post",route: "/users",controller: UserController,action: "save"},
    { method: "post",route: "/users/create",controller: UserController,action: "createUser"}, 
    { method: "post",route: "/users/auth",controller: UserController,action: "auth"}, 
    { method: "delete",route: "/users/:id",controller: UserController,action: "remove"},

    { method: "get",route: "/relatorio", controller: RelatorioController, action: "all"}, 
    { method: "get",route: "/relatorio/:id",controller: RelatorioController,action: "one"},
    { method: "post",route: "/relatorio",controller: RelatorioController,action: "save"},
    { method: "delete",route: "/relatorio/:id",controller: RelatorioController,action: "remove"},

    { method: "get",route: "/agente", controller: AgenteController, action: "all"}, 
    { method: "get",route: "/agente/:id",controller: AgenteController,action: "one"},
    { method: "post",route: "/agente",controller: AgenteController,action: "save"},
    { method: "delete",route: "/agente/:id",controller: AgenteController,action: "remove"},

    { method: "get",route: "/pergunta", controller: PerguntaController, action: "all"}, 
    { method: "get",route: "/pergunta/:id",controller: PerguntaController,action: "one"},
    { method: "post",route: "/pergunta",controller: PerguntaController,action: "save"},
    { method: "delete",route: "/pergunta/:id",controller: PerguntaController,action: "remove"},

    { method: "get",route: "/setor", controller: SetorController, action: "all"}, 
    { method: "get",route: "/setor/:id",controller: SetorController,action: "one"},
    { method: "post",route: "/setor",controller: SetorController,action: "save"},
    { method: "delete",route: "/setor/:id",controller: SetorController,action: "remove"},

    { method: "get",route: "/resposta/:relatorioUid/all", controller: RespostaController, action: "all"}, 
    { method: "post",route: "/resposta",controller: RespostaController,action: "save"},
    { method: "delete",route: "/resposta/:id",controller: RespostaController,action: "remove"}
];