import { Entity, ManyToOne, OneToMany } from "typeorm";
import { BaseEntity } from "./BaseEntity";
import {Column} from "typeorm";
import  Agente  from "./Agente";
import { TipoEnum } from "./enum/TipoEnum";
import { TipoVeiculoEnum } from "./enum/TipoVeiculoEnum";
import { StatusEnum } from "./enum/StatusEnum";
import Pergunta from "./Pergunta";

@Entity({name : 'Relatorio'})
export default class Relatorio extends BaseEntity{
    @Column({type: 'integer', nullable: true})
    numero: number;

    @Column({type: 'integer', nullable: true})
    ano: number;

    @Column({nullable: true})
    horaInicial: string;

    @Column({default: true, nullable: true})
    conduziuViatura: Boolean;

    @Column({ type: "varchar", length: 20, nullable: true})
    numeroViatura: string;

    @Column({type: 'timestamp', nullable: true})
    dataRecebido: Date;

    @Column({ type: "varchar", length: 10, nullable: true})
    numeroFicha: string;

    @Column({ type: "varchar", length: 11, nullable: true})
    numeroAit: string;

    @Column({nullable: true})
    horaFinal: string;

    @Column({ type: "varchar", length: 200, nullable: true})
    local: string;

    @Column({ type: "varchar", length: 500, nullable: true})
    outrosRelatos: string;

    @Column({ type: "varchar", length: 100, nullable: true})
    constatacao: string;

    @Column({ type: "varchar", length: 10, nullable: true})
    setor: string;

    @ManyToOne(() => Agente, {eager: true})
    agente: Agente;

    @Column()
    tipo: TipoEnum;

    @Column()
    statusRelatorio: StatusEnum;

    @Column({nullable: true})
    tipoVeiculo: TipoVeiculoEnum;

    // @OneToMany(type => Pergunta, pergunta => pergunta.uid)
    // perguntas: Pergunta[];

    @ManyToOne(() => Pergunta, {eager: true, nullable: true})
    pergunta: Pergunta;
}