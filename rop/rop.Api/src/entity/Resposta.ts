import { Entity, ManyToOne } from "typeorm";
import { BaseEntity } from "./BaseEntity";
import {Column} from "typeorm";
import Pergunta from "./Pergunta";
import Relatorio from "./Relatorio";

@Entity({name : 'Resposta'})
export default class Resposta extends BaseEntity{

    @Column({ type: 'varchar' })
    resposta: boolean;

    @ManyToOne(() => Pergunta, {eager: true})
    pergunta: Pergunta;

    @ManyToOne(() => Relatorio, {eager: true, nullable: true})
    relatorio: Relatorio;
}