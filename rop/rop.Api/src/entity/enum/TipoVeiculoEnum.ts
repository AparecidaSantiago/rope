export enum TipoVeiculoEnum{
    Escolar = 1,
    Ônibus = 2,
    Mototaxi = 3,
    Motofrete = 4,
    Turismo = 5,
    Táxi = 6
}