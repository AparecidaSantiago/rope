import {Entity, Column} from "typeorm";
import { BaseEntity } from "./BaseEntity"

@Entity({name: 'User'})
export class User extends BaseEntity{

    @Column({type: 'varchar', length: 20})
    username: string;

    @Column({default: false})
    isSuperuser: Boolean;

    @Column({ type: "varchar", length: 100})
    senha: string;

    @Column({default: true})
    ativo: Boolean;

    @Column({ default: false })
    isRoot: boolean;

}
