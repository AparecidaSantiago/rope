import { Column, Entity } from "typeorm";
import { BaseEntity } from "./BaseEntity";

@Entity({name : 'Setor'})
export default class Setor extends BaseEntity{

    @Column({ type: 'varchar' })
    descricao: string;
}