import { Entity, ManyToOne } from "typeorm";
import { BaseEntity } from "./BaseEntity";
import {Column} from "typeorm";
import Relatorio from "./Relatorio";

@Entity({name : 'Pergunta'})
export default class Pergunta extends BaseEntity{

    @Column({ type: 'varchar', length: 200 })
    descricao: string;

    @Column({ type: 'text', nullable: true })
    opcoes: string;
}