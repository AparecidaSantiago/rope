import {Entity, Column} from "typeorm";
import { BaseEntity } from "./BaseEntity"

@Entity({name: 'Agente'})
export default class Agente extends BaseEntity{
    @Column({type: 'varchar', length: 200})
    nome: string;

    @Column({type: 'varchar', length: 10})
    matricula: string;

    @Column({type: 'varchar', length: 200, nullable: true})
    assinatura: string;
}