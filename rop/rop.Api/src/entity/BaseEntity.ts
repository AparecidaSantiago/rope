import { CreateDateColumn, Column,PrimaryGeneratedColumn } from "typeorm";

export abstract class BaseEntity {
    @PrimaryGeneratedColumn("uuid")
    uid: string;

    @CreateDateColumn({type: 'timestamp'})
    dataCriacao: Date;

    @Column({ default: false })
    deleted: boolean;
}