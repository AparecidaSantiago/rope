import Pergunta from "../entity/Pergunta";
import { BaseController } from "./BaseController";
import { Request } from "express";
import Resposta from "../entity/Resposta";

export class RespostaController extends BaseController<Resposta> {
    constructor(){
        super(Resposta, false);
    }

    async all(request: Request){
        let { relatorioUid } = request.params;

        if(!relatorioUid)
            return { status: 400, message: "Informe o código do relatório"};

        this.repository.find({
            resposta: relatorioUid
        });
    }

    async save(request: Request){
        let _resposta = <Resposta>request.body;

        super.isRequired(_resposta.pergunta, 'A pergunta precisa ser informada');
        super.isRequired(_resposta.resposta, 'Informe a resposta da pergunta');
        super.isRequired(_resposta.relatorio, 'O relatório precisa ser preenchido');
        
        return super.save(_resposta, request);
    }
}