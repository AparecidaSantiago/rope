import Relatorio from "../entity/Relatorio";
import { BaseController } from "./BaseController";
import { Request } from "express";
import { StatusEnum } from "../entity/enum/StatusEnum";

export class RelatorioController extends BaseController<Relatorio> {
    constructor(){
        super(Relatorio, false);
    }

    async save(request: Request){
        let _relatorio = <Relatorio>request.body;
        super.isRequired(_relatorio.horaInicial, 'Hora inicial é obrigatório');
        super.isRequired(_relatorio.dataRecebido, 'Data recebido é obrigatório');
        super.isRequired(_relatorio.agente, 'Agente é obrigatório');
        super.isRequired(_relatorio.tipo, 'Tipo da ação é obrigatório');

        if(!request.uid)
            _relatorio.statusRelatorio = StatusEnum.Pendente;

        return super.save(_relatorio, request);
    }

    // async createRelatorio(request: Request) {
    //     let _relatorio = <Relatorio>request.body;
    
    //     super.isRequired(_relatorio.horaInicial, 'Hora inicial é obrigatório');
    //     super.isRequired(_relatorio.conduziuViatura, 'Conduziu viatura é obrigatório');
    //     super.isRequired(_relatorio.dataRecebido, 'Data recebido é obrigatório');
    //     super.isRequired(_relatorio.agente, 'Agente é obrigatório');
    //     super.isRequired(_relatorio.tipo, 'Tipo da ação é obrigatório');

    //     return super.save(_relatorio, request, true);
    //   }
}