import {getRepository, Repository} from "typeorm";
import { Request } from "express";
import {User} from "../entity/User";
import { BaseController } from "./BaseController";
import * as md5 from 'md5';
import { sign } from 'jsonwebtoken';
import config from "../configuration/config";

export class UserController extends BaseController<User> {
    constructor(){
        super(User);
    }

    async auth(request: Request){
        let {username, senha} = request.body;

        if(!username || !senha)
            return { status: 400, message: "Informe o usuário e senha para efetuar o login"};
        
        let user = await this.repository.findOne({username: username, senha: md5(senha)});
        if(user){
            let _payload = {
                uid: user.uid,
                username: user.username
            }
            return {
                status: 200,
                message: {
                    user: _payload,
                    token: sign({
                        ..._payload, 
                        tm: new Date().getTime()
                    }, config.secretKey)
                }
            }
        }else
            return { status: 404, message: "Usuário ou senha inválidos"};
        
    }

    async createUser(request: Request){
        let {username, senha} = request.body;
        super.isRequired(username, 'Informe o nome');
        super.isRequired(senha, 'Informe a senha');

        //monta o objeto
        let _user = new User();
        _user.username = username;

        if(senha)
            _user.senha = md5(senha);

        _user.ativo = true;

        return super.save(_user, request);
    }

    async save(request: Request){
        let _user = <User>request.body;
        super.isRequired(_user.username, 'Username é obrigatório');
        super.isRequired(_user.senha, 'Senha é obrigatório');
        return super.save(_user, request);
    }
}