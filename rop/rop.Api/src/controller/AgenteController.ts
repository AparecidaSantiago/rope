import Agente from "../entity/Agente";
import { BaseController } from "./BaseController";
import { Request } from "express";

export class AgenteController extends BaseController<Agente> {
    constructor(){
        super(Agente);
    }

    async save(request: Request){
        let _agente = <Agente>request.body;
        super.isRequired(_agente.nome, 'Nome é obrigatório');
        super.isRequired(_agente.matricula, 'Matrícula é obrigatório');

        return super.save(_agente, request);
    }
}