import Pergunta from "../entity/Pergunta";
import { BaseController } from "./BaseController";
import { Request } from "express";
import Setor from "../entity/Setor";

export class SetorController extends BaseController<Setor> {
    constructor(){
        super(Setor, true);
    }

    async save(request: Request){
        let _setor = <Setor>request.body;
        super.isRequired(_setor.descricao, 'Descrição é obrigatória');
        
        return super.save(_setor, request);
    }
}