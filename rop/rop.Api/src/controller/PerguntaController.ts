import Pergunta from "../entity/Pergunta";
import { BaseController } from "./BaseController";
import { Request } from "express";

export class PerguntaController extends BaseController<Pergunta> {
    constructor(){
        super(Pergunta, true);
    }

    async save(request: Request){
        let _pergunta = <Pergunta>request.body;
        super.isRequired(_pergunta.descricao, 'Descrição é obrigatória');
        
        return super.save(_pergunta, request);
    }
}