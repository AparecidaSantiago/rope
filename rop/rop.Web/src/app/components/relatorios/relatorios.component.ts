import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { RelatorioService } from 'src/app/services/relatorio.service';
import { IRelatorio } from '../../interfaces/IRelatorio';

@Component({
  selector: 'app-relatorios',
  templateUrl: './relatorios.component.html',
  styleUrls: ['./relatorios.component.scss']
})
export class RelatoriosComponent implements OnInit {
  columns: string[] = ['Relatorios'];
  dataSource: MatTableDataSource<IRelatorio> = new MatTableDataSource<IRelatorio>();

  constructor() { }

  ngOnInit(): void {
  }

}
