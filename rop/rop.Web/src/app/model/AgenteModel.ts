import { BaseModel } from './BaseModel';

export class AgenteModel extends BaseModel {
    nome!: string;
    matricula!: string;
}