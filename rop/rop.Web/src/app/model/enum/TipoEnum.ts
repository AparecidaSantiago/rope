export enum TipoEnum{
    Demanda = 1,
    Ação = 2
}

export namespace TipoEnum {
    export function values() {
        return Object.keys(TipoEnum).filter(
        (type) => isNaN(<any>type) && type !== 'values'
        );
    }
}