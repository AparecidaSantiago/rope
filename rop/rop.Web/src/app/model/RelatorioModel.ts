import { AgenteModel } from './AgenteModel';
import { BaseModel } from './BaseModel';
import { StatusEnum } from './enum/StatusEnum';
import { TipoEnum } from './enum/TipoEnum';
import { TipoVeiculoEnum } from './enum/TipoVeiculoEnum';
import { PerguntaModel } from './PerguntaModel';
import { SetorModel } from './SetorModel';

export class RelatorioModel extends BaseModel {
    numero!: number;
    ano!: number;
    horaInicial!: string;
    conduziuViatura!: Boolean;
    numeroViatura!: string;
    dataRecebido!: Date;
    numeroFicha!: string;
    numeroAit!: string;
    horaFinal!: string;
    local!: string;
    outrosRelatos!: string;
    constatacao!: string;
    agente!: AgenteModel;
    tipo!: TipoEnum;
    statusRelatorio!: StatusEnum;
    tipoVeiculo!: TipoVeiculoEnum;
    pergunta!: PerguntaModel;
    setor!: SetorModel;

    constructor(){
        super();
        this.agente = new AgenteModel();
        this.pergunta = new PerguntaModel();
        this.setor = new SetorModel();
    }
}