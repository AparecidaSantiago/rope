import { BaseModel } from './BaseModel';

export class PerguntaModel extends BaseModel {
    descricao!: string;
    opcoes!: string;
}