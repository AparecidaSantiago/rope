import { IInterface } from './IInterface';

export interface IAgente extends IInterface{
    nome: string;
    matricula: string;
    assinatura: string;
}