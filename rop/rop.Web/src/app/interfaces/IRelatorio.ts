import { IInterface } from './IInterface';

export interface IRelatorio extends IInterface{
    nome: string,
    agenteMatricula: string;
}