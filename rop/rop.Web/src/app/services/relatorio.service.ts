import { Injectable } from '@angular/core';
import { BaseService } from '../base/base.service';
import { RelatorioModel } from '../model/RelatorioModel';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class RelatorioService extends BaseService<RelatorioModel>{

  constructor(public http: HttpService) {
    super('relatorio', http);
   }
}
