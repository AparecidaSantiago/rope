import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { BaseService } from '../base/base.service';
import { IResultHttp } from '../interfaces/IResultHttp';
import { HttpService } from './http.service';

@Injectable({
    providedIn: 'root'
})

export class UserService extends BaseService<any>{

    private loginSobject = new Subject<boolean>();

    constructor(public http: HttpService){
        super('users', http);
    }

    login(username: string, senha: string): Promise<IResultHttp>{
        return this.http.post(`${environment.url_api}/users/auth`, {username, senha});
    }

    configureLogin(obj:any): void{
        const { token, user } = obj.data;
        localStorage.setItem('rop:token', token);
        localStorage.setItem('rop:user', JSON.stringify(user));
        this.loginSobject.next(this.isStaticLogged);
    }

    get isLogged(): Observable<boolean>{
        return this.loginSobject.asObservable();
    }

    get isStaticLogged(): boolean{
        return !!localStorage.getItem('rop:token');
    }
}