import { Injectable } from '@angular/core';
import { BaseService } from '../base/base.service';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class SetorService extends BaseService<any>{

  constructor(public http: HttpService) {
    super('setor', http);
   }
}
