import { Injectable } from '@angular/core';
import { BaseService } from '../base/base.service';
import { IAgente } from '../interfaces/IAgente';
import { AgenteModel } from '../model/AgenteModel';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class AgenteService extends BaseService<IAgente>{

  constructor(public http: HttpService) {
    super('agente', http);
   }
}
