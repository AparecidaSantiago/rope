import { Constants } from './../../shared/constants';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { IAgente } from 'src/app/interfaces/IAgente';
import { AgenteService } from 'src/app/services/agente.service';
import Swal from 'sweetalert2';
import { MatPaginator } from '@angular/material/paginator';

@Component({
  selector: 'app-agentes',
  templateUrl: './agentesList.component.html',
  styleUrls: ['./agentesList.component.scss']
})
export class AgentesListComponent implements OnInit {
  columns: string[] = ['Nome', 'Matrícula', 'uid'];
  dataSource!: MatTableDataSource<IAgente>;
  @ViewChild(MatPaginator)
  paginator!: MatPaginator;

  constructor(private agenteSrv: AgenteService) { }

  async ngOnInit() {
    this.bind();
  }

  async bind() {
    const agentes = await this.agenteSrv.getAll();
    this.dataSource = new MatTableDataSource(agentes.data);
    this.dataSource.paginator = this.paginator;
  }

  filter(value: string) {
    this.dataSource.filter = value.trim().toLowerCase();
  }

  async delete(agente: IAgente): Promise<void> {
    const options: any = { ...Constants.confirm_swal_options, text: `Deseja realmente excluir o agente ${agente.nome}` };
    const { value } = await Swal.fire(options);
    if (value) {
      const resul = await this.agenteSrv.delete(agente.uid);
      if (resul.success) {
        this.bind();
      }
    }
  }


}
