import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { IRelatorio } from 'src/app/interfaces/IRelatorio';
import { RelatorioModel } from 'src/app/model/RelatorioModel';
import { RelatorioService } from 'src/app/services/relatorio.service';
import { Constants } from 'src/app/shared/constants';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-relatorios-list',
  templateUrl: './relatorios-list.component.html',
  styleUrls: ['./relatorios-list.component.scss']
})
export class RelatoriosListComponent implements OnInit {
  columns: string[] = ['Setor', 'HoraInicial', 'Agente', 'uid'];
  dataSource!: MatTableDataSource<IRelatorio>;
  @ViewChild(MatPaginator)
  paginator!: MatPaginator;

  constructor(private relatorioSrv: RelatorioService) { }

  async ngOnInit() {
    this.bind();
  }

  async bind() {
    const relatorios = await this.relatorioSrv.getAll();
    this.dataSource = new MatTableDataSource(relatorios.data);
    this.dataSource.paginator = this.paginator;
  }

  filter(value: string) {
    this.dataSource.filter = value.trim().toLowerCase();
  }

  async delete(relatorio: RelatorioModel): Promise<void> {
    const options: any = { ...Constants.confirm_swal_options, text: `Deseja realmente excluir o relatório ${relatorio.horaInicial}` };
    const { value } = await Swal.fire(options);
    if (value) {
      const resul = await this.relatorioSrv.delete(relatorio.uid);
      if (resul.success) {
        this.bind();
      }
    }
  }

}
