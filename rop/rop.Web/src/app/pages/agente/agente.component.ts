import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { IAgente } from 'src/app/interfaces/IAgente';
import { AgenteModel } from 'src/app/model/AgenteModel';
import { AgenteService } from 'src/app/services/agente.service';

@Component({
  selector: 'app-agente',
  templateUrl: './agente.component.html',
  styleUrls: ['./agente.component.scss']
})
export class AgenteComponent implements OnInit {

  agente: AgenteModel = new AgenteModel();

  constructor(
    private agenteService: AgenteService,
    private matSnack: MatSnackBar,
    private router: Router,
    private activate: ActivatedRoute
  ) { }

  ngOnInit() {
    this.activate.params.subscribe(p => this.getId(p.id));
  }

  async getId(uid: string): Promise<void> {
    if (uid === 'new') { return; }
    const result = await this.agenteService.getById(uid);
    this.agente = result.data as AgenteModel;
  }

  async save(): Promise<void> {
    const result = await this.agenteService.post(this.agente as IAgente);
    if (result.success) {
      this.matSnack.open('Agente salvo com sucesso', undefined, { duration: 3000 });
      this.router.navigateByUrl('/agentes');
    }
  }


}
