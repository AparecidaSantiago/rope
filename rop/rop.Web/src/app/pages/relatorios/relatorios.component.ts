import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { TipoEnum } from 'src/app/model/enum/TipoEnum';
import { RelatorioModel } from 'src/app/model/RelatorioModel';
import { SetorModel } from 'src/app/model/SetorModel';
import { RelatorioService } from 'src/app/services/relatorio.service';
import { SetorService } from 'src/app/services/setor.service';

@Component({
  selector: 'app-relatorio',
  templateUrl: './relatorios.component.html',
  styleUrls: ['./relatorios.component.scss']
})
export class RelatoriosComponent implements OnInit {
  relatorio: RelatorioModel = new RelatorioModel();
  setores!: Array<SetorModel>;
  public tipoEnum = TipoEnum;

  constructor(
    private setorSrv: SetorService,
    private relatorioSrv: RelatorioService,
    private matSnack: MatSnackBar,
    private router: Router,
    private active: ActivatedRoute
  ) { }

  ngOnInit() {
    this.active.params.subscribe(p => this.getId(p.id));
    this.bindSetores();
  }

  async bindSetores(): Promise<void> {
    const result = await this.setorSrv.getAll();
    if (result.success) {
      this.setores = result.data as Array<SetorModel>;
    }
  }

  async getId(uid: string): Promise<void> {
    if (uid === 'new') { return; }
    const result = await this.relatorioSrv.getById(uid);
    this.relatorio = result.data as RelatorioModel;
  }

  async save(): Promise<void> {
    console.log(this.relatorio);
    const result = await this.relatorioSrv.post(this.relatorio);
    if (result.success) {
      this.matSnack.open('Relatório salvo com sucesso', undefined, { duration: 3000 });
      this.router.navigateByUrl('/relatorios');
    }
  }

}
