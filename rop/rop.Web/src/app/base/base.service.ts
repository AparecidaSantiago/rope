import { CoreEnvironment } from '@angular/compiler/src/compiler_facade_interface';
import { environment } from '../../environments/environment';
import { IResultHttp } from '../interfaces/IResultHttp';
import { HttpService } from '../services/http.service';

export abstract class BaseService<T> {

    urlBase: string = '';
    constructor(   
        public url: string, 
        public http: HttpService){
            this.urlBase = `${environment.url_api}/${this.url}`;
    }

    public getAll(): Promise<IResultHttp> {
        return this.http.get(this.urlBase);
    }

    public getById(uid: string): Promise<IResultHttp> {
        return this.http.get(`${this.urlBase}/${uid}`);
    }

    public post(model: T): Promise<IResultHttp> {
        return this.http.post(this.urlBase, model);
    }

    public delete(uid: string): Promise<IResultHttp> {
        return this.http.delete(`${this.urlBase}/${uid}`);
    }
}