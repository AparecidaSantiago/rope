import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AgentesListComponent } from './pages/agentesList/agentesList.component';
import { AgenteComponent } from './pages/agente/agente.component';
import { HomeComponent } from './pages/home/home.component';
import { RelatoriosComponent } from './pages/relatorios/relatorios.component';
import { LoginComponent } from './pages/login/login.component';
import { AdminGuard } from './shared/admin.guard';
import { RelatoriosListComponent } from './pages/relatorios-list/relatorios-list.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: '/home' },
  { path: 'home', component: HomeComponent, canActivate:[AdminGuard] },
  { path: 'agentes', component: AgentesListComponent,  canActivate:[AdminGuard] },
  { path: 'agentes/:id', component: AgenteComponent,  canActivate:[AdminGuard] },
  { path: 'relatorios', component: RelatoriosListComponent,  canActivate:[AdminGuard] },
  { path: 'relatorios/:id', component: RelatoriosComponent,  canActivate:[AdminGuard] },
  { path: 'login', component: LoginComponent },
 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
